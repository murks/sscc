local Cell = CLASS('Cell')

local cellText = {
  l = "Valley, $50M,\n+$50M/turn,\n-1 turn",
  m = "Regular, $100M,\n+$50M/turn,\n-1 turn",
  h = "High ground, $200M,\n+$50M/turn,\n-1 turn"
}


function Cell:initialize(x, y, elevation, content, owner, sunk)
  self.e = elevation or 'm' -- elevation: low, mid, high
  --self.c = content or 'e' -- content: empty, factory
  self.o = owner or 'n' -- owner: n, 1, 2
  self.x = x
  self.y = y
  self.sunk = sunk or false
end


function Cell:click(x, y)
  if isPointInsideRect(x, y, self.x, self.y, CELLSIZE, CELLSIZE) then
    if P[ACTIVEPLAYER].cash >= FACTORY[self.e].cost and self.o == 'n' and not self.sunk then
      self.o = ACTIVEPLAYER
      P[ACTIVEPLAYER].cash = P[ACTIVEPLAYER].cash - FACTORY[self.e].cost
      PHASE[CURRENTPHASE] = PHASE[CURRENTPHASE] -1
      SFX.building:play()
      action()
    end
  end
end


function Cell:checkHover(x, y)
  if isPointInsideRect(x, y, self.x, self.y, CELLSIZE, CELLSIZE) then
    love.graphics.print(cellText[self.e], 700, 600)
  end
end


function Cell:draw()
  local color = COLOR[self.e]
  love.graphics.setColor(color)

  local drawSize = CELLSIZE - 1

  love.graphics.rectangle(
    'fill',
    self.x,
    self.y,
    drawSize,
    drawSize
  )

  if self.o ~= 'n' then
    love.graphics.draw(FACTORYIMG, self.x +8, self.y -14)
  end

  if self.sunk then
    love.graphics.setColor(COLOR.b)
    love.graphics.rectangle(
      'fill',
      self.x,
      self.y,
      drawSize,
      drawSize
    )
  end
end

return Cell