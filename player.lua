local Player = CLASS('Player')

function Player:initialize(name, x, y, cash, income, levels, id)
  self.id = id
  self.name = name
  self.x = x
  self.y = y
  self.cash = cash or 100
  self.income = income or 0
  self.levels = levels or 2
  --self.isActive = isActive or false
end

function Player:draw() -- draw tower starting with base
  if ACTIVEPLAYER == self.id then
    love.graphics.setColor(1, 1, 1, 1)
  else
    love.graphics.setColor(.8, .8, .8, .9)
  end
  -- top h = 74, w = 140
  -- mid h = 82, w = 182
  -- base h = 86, w = 188
  love.graphics.draw(T.base, self.x, self.y)

  local i = 1
  while i <= self.levels do
    love.graphics.draw(T.mid, self.x + 3, self.y - i * 82)
    i = i + 1
  end
  
  love.graphics.draw(T.top, self.x + 24, self.y - (i-1) * 82 - 74)
  love.graphics.print(self.name, self.x + 24 + 20, self.y - (i-1) * 82 - 74 + 25)
  love.graphics.setColor(1, 1, 1, 1)
end

return Player