local Cell = require 'cell'

local Grid = CLASS('Grid')

local g = {} -- grid instance

Grid.static.offsetX = 700
Grid.static.offsetY = 200

function Grid:initialize()
  for y =1,4 do
    g[y] = {}
    for x=1,4 do
      local cx = (x - 1) * CELLSIZE + Grid.static.offsetX
      local cy = (y - 1) * CELLSIZE + Grid.static.offsetY
      g[y][x] = Cell:new(cx, cy)
    end
  end

  g[1][1].e = 'l'
  g[2][2].e = 'l'
  g[3][3].e = 'l'
  g[4][4].e = 'l'

  g[1][4].e = 'h'
  g[1][3].e = 'h'
  g[4][1].e = 'h'
  g[4][2].e = 'h'
end


function Grid:flood(elevation)
  for y =1,4 do
    for x=1,4 do
      if g[y][x].e == elevation then
        g[y][x].sunk = true
      end
    end
  end  
end


function Grid:checkClick(px, py)
  if isPointInsideRect(px, py, Grid.static.offsetX, Grid.static.offsetY, 4*CELLSIZE, 4*CELLSIZE) then
    for y =1,4 do
      for x=1,4 do
        g[y][x]:click(px, py)
      end
    end
  end
end


function Grid:checkHover(px, py)
  if isPointInsideRect(px, py, Grid.static.offsetX, Grid.static.offsetY, 4*CELLSIZE, 4*CELLSIZE) then
    for y =1,4 do
      for x=1,4 do
        g[y][x]:checkHover(px, py)
      end
    end
  end
end


function Grid:draw()
  for y = 1,4 do
    for x= 1,4 do
      g[y][x]:draw()
    end
  end
end

function Grid:calculateIncome()
  local s1 = 0
  local s2 = 0

  for y = 1,4 do
    for x = 1,4 do
      if not g[y][x].sunk then
        local income = 50
        if g[y][x].o == 1 then
          s1 = s1 + income
        elseif g[y][x].o == 2 then
          s2 = s2 + income
        end
      end
    end
  end
  return s1, s2
end

return Grid