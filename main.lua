-- Something, Something, Climate Change

-- package love file using: zip -9 -r SSCC-pre1.love . -x \*.git\*

--[[ to fix:
    1. make factories distinguishable
    2. playtest
    3. End earlier
]]--

CLASS = require 'lib/middleclass'
TWEEN = require 'lib/tween'
INSPECT = require 'lib/inspect'
local suit = require 'lib/suit'
STATE = require 'lib/gamestate'

local Player = require 'player'
local Grid = require 'grid'

-- gamestate tables
TITLE = {}
GAME = {}
END = {}


COLOR = {
  a = {180/255, 58/255, 23/255}, -- apocalypse
  h = {180/255, 112/255, 23/255}, -- high
  m = {192/255, 163/255, 23/255}, -- med
  l = {153/255, 180/255, 52/255}, -- low
  s = {74/255, 126/255, 31/255}, -- sub
  b = {162/255, 157/255, 221/255, 0.4} -- water
}

CELLSIZE = 90

FACTORYIMG = love.graphics.newImage('img/Fabrik.png')
local bgimg = love.graphics.newImage('img/Hintergrund2.png')
local researchimg = love.graphics.newImage('img/Button_Forschung.png')
local waterimg = love.graphics.newImage('img/Wasser.png')
local startimg = love.graphics.newImage('img/Start_screen.png')
local endimg = love.graphics.newImage('img/End_Screen.png')

T = { -- Tower images
  top = love.graphics.newImage('img/Hochhausdach.png'), -- h = 74, w = 140
  mid = love.graphics.newImage('img/Hochhausebene.png'), -- h = 82, w = 182
  base = love.graphics.newImage('img/Hochhausbasis.png'), -- h = 86, w = 188
}

M = { -- music
  love.audio.newSource('music/building blocks.mp3', 'stream'),
  love.audio.newSource('music/building factories.mp3', 'stream'),
  love.audio.newSource('music/water front building.mp3', 'stream'),
}

SFX = {
  building = love.audio.newSource('music/building.wav', 'static'), -- build
  draw = love.audio.newSource('music/draw.wav', 'static'), --
  sth1 = love.audio.newSource('music/sth 1.wav', 'static'), --
  sth2 = love.audio.newSource('music/sth 2.wav', 'static'), -- game start
  turn = love.audio.newSource('music/turn.wav', 'static'), -- turn
  water = love.audio.newSource('music/water.wav', 'static'), -- water rising
  winner = love.audio.newSource('music/winner.wav', 'static'), -- end
}

P = {} -- players

PHASE = {}

FACTORY = {
  l = {cost = 50},
  m = {cost = 100},
  h = {cost = 200},
}

local research = {
  turns = 2,
  cost = 200
}

TOWERCOST = 100

MUSIC = M[love.math.random(1,3)]

local arrow = {}
local water = {}

function newGame()
  PHASE = {
    3, -- nix
    3, -- l flodded
    3, -- m flodded
    3, -- h flooded
    1,
    1
  }

  arrow = {
    img = love.graphics.newImage('img/arrow.png'),
    x = 1180,
    y = 605, -- increment by 120
  }

  water = {
    img = waterimg,
    x = 0,
    y = 720,
  }

  ACTIVEPLAYER = 1
  CURRENTPHASE = 1
  P[1] = Player:new("Tower 1", 90, 570, 200, 0, 1, 1)
  P[2] = Player:new("Tower 2", 370, 570, 200, 0, 1, 2)
  grid = Grid:new()
  MUSIC = M[love.math.random(1,3)]
  MUSIC:setLooping(true)
  MUSIC:play()
end


function action()
  SFX.turn:play()
  local s1, s2 = Grid:calculateIncome()
  P[1].income = s1
  P[2].income = s2
  P[1].cash = P[1].cash + P[1].income
  P[2].cash = P[2].cash + P[2].income

  if ACTIVEPLAYER == 1 then
    ACTIVEPLAYER = 2
  else
    ACTIVEPLAYER = 1
  end

  PHASE[CURRENTPHASE] = PHASE[CURRENTPHASE] - 1
  if PHASE[CURRENTPHASE] < 1 then
    SFX.water:play()
    if CURRENTPHASE < 6 then
      CURRENTPHASE = CURRENTPHASE + 1
      water.y = water.y - 120
      if CURRENTPHASE == 2 then
        --Grid:flood('l')
        arrow.y = arrow.y - 120
      elseif CURRENTPHASE == 3 then
        Grid:flood('l')
        arrow.y = arrow.y - 120
      elseif CURRENTPHASE == 4 then
        Grid:flood('m')
        arrow.y = arrow.y - 120
      elseif CURRENTPHASE == 5 then
        Grid:flood('h')
        arrow.y = arrow.y - 120
      elseif CURRENTPHASE == 5 then
        arrow.y = arrow.y - 120
      end
      -- after flooding to have correct income display
      local s1, s2 = Grid:calculateIncome()
      P[1].income = s1
      P[2].income = s2
      P[1].cash = P[1].cash + P[1].income
      P[2].cash = P[2].cash + P[2].income
    else
      -- DOOM
      STATE.switch(END)
    end
  end
end


function doResearch()
  if P[ACTIVEPLAYER].cash >= research.cost then
    SFX.sth1:play()
    P[ACTIVEPLAYER].cash = P[ACTIVEPLAYER].cash - research.cost
    PHASE[CURRENTPHASE] = PHASE[CURRENTPHASE] + research.turns
    action()
  end
end


function towerGrow()
  if P[ACTIVEPLAYER].cash >= TOWERCOST then
    P[ACTIVEPLAYER].cash = P[ACTIVEPLAYER].cash - TOWERCOST
    P[ACTIVEPLAYER].levels = P[ACTIVEPLAYER].levels + 1
    action()
  end
end


function isPointInsideRect(px, py, x, y, w, h)
  return px > x
  and px < x + w
  and py > y
  and py < y + h
end


function drawMeter(x, y, w, h)
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.setColor(COLOR.a)
  love.graphics.rectangle('fill', x, y, w, h)
  love.graphics.setColor(COLOR.h)
  love.graphics.rectangle('fill', x, y+h, w, h)
  love.graphics.setColor(COLOR.m)
  love.graphics.rectangle('fill', x, y+2*h, w, h)
  love.graphics.setColor(COLOR.l)
  love.graphics.rectangle('fill', x, y+3*h, w, h)
  love.graphics.setColor(COLOR.s)
  love.graphics.rectangle('fill', x, y+4*h, w, h)
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(arrow.img, arrow.x, arrow.y)
end


function love.mousepressed(x, y, button)
  if button == 1 then -- left button
    Grid:checkClick(x, y)
  end
end


function love.keypressed(key)
  suit.keypressed(key)
  if key == 'escape' or key == 'q' then love.event.quit() end
end


function love.load(arg, unfilteredArg)
  if arg[#arg] == "-debug" then require("mobdebug").start() end

  STATE.registerEvents()
  STATE.switch(TITLE)

  love.graphics.setBackgroundColor(1, 1, 1)
  FONT = {
    regular = love.graphics.newFont(24),
    huge = love.graphics.newFont(50)
  }
  love.graphics.setFont(FONT.regular)
  suit.theme.color.normal.fg = {1, 1, 1}
  --suit.theme.color.hovered.bg = {0.8, 0.6, 0.7}
  --newGame()
end


function TITLE.enter()
  MUSIC:stop()
end


function TITLE.update(dt)
  startButton = suit.Button('START', {font = FONT.huge}, 850,600, 200,50)
  if startButton.hit then STATE.switch(GAME) end
end


function TITLE.draw()
  love.graphics.draw(startimg)
  suit.draw()
end


function END.enter()
  if P[1].levels == P[2].levels then
    SFX.draw:play()
  else
    SFX.winner:play()
  end
end


function END.update(dt)
  backButton = suit.Button('BACK', {font = FONT.huge}, 1000,100, 200,50)
  if backButton.hit then STATE.switch(TITLE) end
end


function END.draw()
  love.graphics.draw(endimg)
  suit.draw()
end


function GAME.enter()
  newGame()
  SFX.sth2:play()
end

function GAME.update(dt)  
  researchButton = suit.ImageButton(researchimg, 700, 30)
  if researchButton.hit then doResearch() end

  passButton = suit.Button('PASS', {font = FONT.huge}, 900,100, 150,60)
  if passButton.hit then action() end

  towerButton = suit.Button('+', {font = FONT.huge}, 300,150, 50,50)
  if towerButton.hit then towerGrow() end

  cashP1Label = suit.Label('$' .. P[1].cash .. 'M\n+ $' .. P[1].income .. 'M / turn', 70, 658)
  cashP2label = suit.Label('$' .. P[2].cash .. 'M\n+ $' .. P[2].income .. 'M / turn', 380, 658)

  turnLabel = suit.Label(PHASE[CURRENTPHASE], {font = FONT.huge}, 1155, 36)
end


function GAME.draw()
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(bgimg)

  for i=1,#P do
    P[i]:draw()
  end

  drawMeter(1140,100, 60,120)

  suit.draw()

  love.graphics.setColor(1, 1, 1, 1)
  --love.graphics.print("Phase: " .. CURRENTPHASE, 900, 60)
  love.graphics.draw(water.img, water.x, water.y)

  grid:draw()
  love.graphics.setColor(1, 1, 1, 1)

  local x, y = love.mouse.getPosition()
  Grid:checkHover(x, y)


  if researchButton.hovered then
    love.graphics.print('Climate Research, $'.. research.cost .. 'M,\ndelay flooding by ' .. research.turns .. ' turns', 700, 600)
  end

  if cashP1Label.hovered then
    love.graphics.print('Player cash and income', 700, 600)
  end

  if towerButton.hovered then
    love.graphics.print('Enlarge your tower for $' .. TOWERCOST .. 'M', 700, 600)
  end

  if turnLabel.hovered then
    love.graphics.print('Turns until next flooding', 700, 600)
  end
end
